Simplify
========

如你所见，这是一个 Hexo 的主题，移植自 [Simplify](https://www.tumblr.com/theme/38176)，代码高亮的样式来自 [Next](https://github.com/iissnan/hexo-theme-next)。

![Simplify](http://ww2.sinaimg.cn/large/0060lm7Tjw1evaj6feclrj31hc0zk45w.jpg)

## 使用

1. 下载后将主题放至 `theme` 文件夹内
2. 更改站点目录下的 `_config.yml` 文件，将 `theme` 选项改为 `simplify`
3. hexo server

### 设置

- NightMode
    是否开启夜间模式 `Ture` 或者 `False`

- AvaterLink
    首页的头像链接，默认为 `/oamges/avatar.png`

- Category
    设置是否在 Archives 页面显示分类，默认为 `Flase`


```
# Header
menu:
  Home: /
    Archives: /archives
    rss: /atom.xml
nightmode: False
avatarlink: /images/avatar.png

# Content
excerpt_link: Read More
category: False
```

## 其它好看的 Tumblr 主题

- Square
  http://www.tumblr.com/theme/39044
- Publisher
  https://www.tumblr.com/theme/38895
- Paper
  https://www.tumblr.com/theme/38481